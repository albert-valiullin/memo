package ru.tatskill.pamyatka.web.dao;

import ru.tatskill.pamyatka.web.model.User;

/**
 * Created by Admin on 02.08.2015.
 */
public interface UserDAO extends DAO<User, Integer>{
}
