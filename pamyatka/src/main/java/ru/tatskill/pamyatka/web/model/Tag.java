package ru.tatskill.pamyatka.web.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Admin on 13.07.2015.
 */
@Entity
@Table(name = "TEMPLATE_TAG")
public class Tag implements Serializable {

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TEMP_TAG_TEMP_ID")
    private Template template;

    @Id
    @Column(name = "TEMP_TAG_TAG", length = 32)
    private String tag;

    public Tag() {
    }

    public Tag(Template template, String tag) {
        this.template = template;
        this.tag = tag;
    }

    public Template getTemplate() {
        return template;
    }

    public void setTemplate(Template template) {
        this.template = template;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tag tag1 = (Tag) o;

        return tag.equals(tag1.tag);

    }

    @Override
    public int hashCode() {
        return tag.hashCode();
    }

    class TagId {

    }
}
