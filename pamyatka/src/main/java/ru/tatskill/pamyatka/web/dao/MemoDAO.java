package ru.tatskill.pamyatka.web.dao;

import ru.tatskill.pamyatka.web.model.Memo;

/**
 * Created by Admin on 02.08.2015.
 */
public interface MemoDAO extends DAO<Memo, Integer> {
}
