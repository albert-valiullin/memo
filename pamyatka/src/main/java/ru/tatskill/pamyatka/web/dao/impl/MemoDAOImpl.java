package ru.tatskill.pamyatka.web.dao.impl;

import org.springframework.stereotype.Repository;
import ru.tatskill.pamyatka.web.dao.MemoDAO;
import ru.tatskill.pamyatka.web.model.Memo;

/**
 * Created by Admin on 02.08.2015.
 */
@Repository
public class MemoDAOImpl extends DAOImpl<Memo, Integer> implements MemoDAO {
}
