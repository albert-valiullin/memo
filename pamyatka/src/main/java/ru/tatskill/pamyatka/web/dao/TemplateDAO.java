package ru.tatskill.pamyatka.web.dao;

import ru.tatskill.pamyatka.web.model.Template;

/**
 * Created by Admin on 02.08.2015.
 */
public interface TemplateDAO extends DAO<Template, Integer> {
}
