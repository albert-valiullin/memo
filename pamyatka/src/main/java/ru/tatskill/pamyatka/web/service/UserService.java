package ru.tatskill.pamyatka.web.service;

import ru.tatskill.pamyatka.web.model.Memo;
import ru.tatskill.pamyatka.web.model.Template;
import ru.tatskill.pamyatka.web.model.User;

import java.util.List;
import java.util.Set;

/**
 * Created by Admin on 02.08.2015.
 */
public interface UserService {
    void create(User user);
    List<User> findAll();
    Set<Memo> fetchMemos(User user);
    Set<Template> fetchTemplates(User user);
}
