package ru.tatskill.pamyatka.web.model;

/**
 * Created by Admin on 08.08.2015.
 */
public class MemoType {
    public static final int UNORDERED_POINTS   = 0,
                            ORDERED_POINTS     = 1,
                            UNORDERED_CHECKBOX = 2,
                            ORDERED_CHECKBOX   = 3;
}
