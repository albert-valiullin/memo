package ru.tatskill.pamyatka.web.dao.impl;

import org.springframework.stereotype.Repository;
import ru.tatskill.pamyatka.web.dao.TemplateDAO;
import ru.tatskill.pamyatka.web.model.Template;

/**
 * Created by Admin on 02.08.2015.
 */
@Repository
public class TemplateDAOImpl extends DAOImpl<Template, Integer> implements TemplateDAO {
}
