package ru.tatskill.pamyatka.web.dao;

import ru.tatskill.pamyatka.web.model.Category;

/**
 * Created by Admin on 02.08.2015.
 */
public interface CategoryDAO extends DAO<Category, Integer> {
}
