package ru.tatskill.pamyatka.web.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.tatskill.pamyatka.web.dao.TagDAO;
import ru.tatskill.pamyatka.web.model.Tag;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Admin on 02.08.2015.
 */
@Repository
@Transactional
public class TagDAOImpl implements TagDAO {
    @Autowired
    private SessionFactory sessionFactory;

    public List<Tag> findAll() {
        return (List<Tag>) sessionFactory.getCurrentSession().createCriteria(Tag.class).list();
    }
}
