package ru.tatskill.pamyatka.web.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

/**
 * Created by Admin on 13.07.2015.
 */
@Entity
@Table(name = "USER")
public class User implements Serializable {
    @Id
    @Column(name = "USER_ID")
    @GeneratedValue
    private Integer id;

    @Column(name = "USER_USERNAME", unique = true, nullable = false, length = 32)
    private String username;

    @Column(name = "USER_EMAIL", unique = true, length = 255)
    private String email;

    @Column(name = "USER_PASSWORD", nullable = false, length = 128)
    private String password;

    @Column(name = "USER_CREATE_DATE")
    private Timestamp createDate;

    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    private Set<Template> templates;

    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    private Set<Memo> memos;

    public User() {
    }

    public User(String username, String email, String password, Timestamp createDate) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.createDate = createDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Set<Template> getTemplates() {
        return templates;
    }

    public void setTemplates(Set<Template> templates) {
        this.templates = templates;
    }

    public Set<Memo> getMemos() {
        return memos;
    }

    public void setMemos(Set<Memo> memos) {
        this.memos = memos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (!username.equals(user.username)) return false;
        return email.equals(user.email);

    }

    @Override
    public int hashCode() {
        int result = username.hashCode();
        result = 31 * result + email.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", createDate=" + createDate +
                '}';
    }
}
