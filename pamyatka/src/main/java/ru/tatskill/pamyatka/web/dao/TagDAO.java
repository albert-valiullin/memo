package ru.tatskill.pamyatka.web.dao;

import ru.tatskill.pamyatka.web.model.Tag;

import java.util.List;

/**
 * Created by Admin on 02.08.2015.
 */
public interface TagDAO {
    List<Tag> findAll();
}
