package ru.tatskill.pamyatka.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tatskill.pamyatka.web.dao.UserDAO;
import ru.tatskill.pamyatka.web.model.Memo;
import ru.tatskill.pamyatka.web.model.Template;
import ru.tatskill.pamyatka.web.model.User;
import ru.tatskill.pamyatka.web.service.UserService;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Set;

/**
 * Created by Admin on 02.08.2015.
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserDAO userDAO;

    public void create(User user) {
        userDAO.create(user);
    }

    public List<User> findAll() {
        return userDAO.findAll();
    }

    public Set<Memo> fetchMemos(User user) {
        try {
            return (Set<Memo>) userDAO.fetch(user, "getMemos");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Set<Template> fetchTemplates(User user) {
        try {
            return (Set<Template>) userDAO.fetch(user, "getTemplates");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }


}
