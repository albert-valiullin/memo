package ru.tatskill.pamyatka.web.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by Admin on 13.07.2015.
 */
@Entity
@Table(name = "TEMPLATE")
public class Template implements Serializable {
    @Id
    @Column(name = "TEMP_ID")
    @GeneratedValue
    private Integer id;

    @Column(name = "TEMP_NAME", nullable = false, length = 64)
    private String name;

    @Column(name = "TEMP_CONTENT", length = 10240)
    private String content;

    @Column(name = "TEMP_COMMENT", length = 1024)
    private String comment;

    @Column(name = "TEMP_PRIVATE", nullable = false, length = 1)
    private boolean isPrivate = true;

    @ManyToOne
    @JoinColumn(name = "TEMP_OWNER_ID")
    private User owner;

    @Column(name = "TEMP_TYPE", nullable = false, columnDefinition = "INT DEFAULT 0")
    private Integer type;

    @Column(name = "TEMP_COPY_COUNT", nullable = false, columnDefinition = "BIGINT DEFAULT 0")
    private Long copyCount;

    @ManyToOne
    @JoinColumn(name = "TEMP_CAT_ID")
    private Category category;

    @OneToMany(mappedBy = "template")
    private Set<Tag> tags;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "USER_TEMPLATE",
            joinColumns = {@JoinColumn(name = "USER_TEMP_TEMP_ID")},
            inverseJoinColumns = {@JoinColumn(name = "USER_TEMP_USER_ID")}
    )
    private Set<User> users;

    public Template() {
    }

    public Template(String name, String content, String comment, Boolean isPrivate, User owner, Integer type, Long copyCount, Category category, Set<Tag> tags) {
        this.name = name;
        this.content = content;
        this.comment = comment;
        this.isPrivate = isPrivate;
        this.owner = owner;
        this.type = type;
        this.copyCount = copyCount;
        this.category = category;
        this.tags = tags;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getCopyCount() {
        return copyCount;
    }

    public void setCopyCount(Long copyCount) {
        this.copyCount = copyCount;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Template template = (Template) o;

        if (isPrivate != template.isPrivate) return false;
        if (!name.equals(template.name)) return false;
        if (!content.equals(template.content)) return false;
        if (comment != null ? !comment.equals(template.comment) : template.comment != null) return false;
        if (!owner.equals(template.owner)) return false;
        if (!type.equals(template.type)) return false;
        return category.equals(template.category);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + content.hashCode();
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + (isPrivate ? 1 : 0);
        result = 31 * result + owner.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + category.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Template{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", content='" + content + '\'' +
                ", comment='" + comment + '\'' +
                ", isPrivate=" + isPrivate +
                ", owner=" + owner +
                ", type=" + type +
                ", copyCount=" + copyCount +
                ", category=" + category +
                ", tags=" + tags +
                ", users=" + users +
                '}';
    }
}
