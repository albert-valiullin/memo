package ru.tatskill.pamyatka.web.dao;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Created by Admin on 03.08.2015.
 */
public interface DAO<T, Id extends Serializable> {
    void create(T entity);
    T findById(Id id);
    void update(T entity);
    void delete(T entity);
    List<T> findAll();
    Object fetch(T entity, String methodName) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException;

}
