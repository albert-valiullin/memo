package ru.tatskill.pamyatka.web.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Admin on 13.07.2015.
 */
@Entity
@Table(name = "MEMO")
public class Memo {
    @Id
    @Column(name = "MEMO_ID")
    @GeneratedValue
    private Integer id;

    @Column(name = "MEMO_NAME", nullable = false, length = 64)
    private String name;

    @Column(name = "MEMO_CONTENT", length = 10240)
    private String content;

    @Column(name = "MEMO_COMMENT", length = 1024)
    private String comment;

    @Column(name = "MEMO_PRIVATE", nullable = false, length = 1)
    private boolean isPrivate = true;

    @ManyToOne
    @JoinColumn(name = "MEMO_USER_ID")
    private User owner;

    @Column(name = "MEMO_TYPE", nullable = false, columnDefinition = "INT DEFAULT 0")
    private Integer type;

    @Column(name = "MEMO_START_DATE")
    private Timestamp startDate;

    @Column(name = "MEMO_COLOR", nullable = false, columnDefinition = "INT DEFAULT 0")
    private Integer color;

    @ManyToOne
    @JoinColumn(name = "MEMO_CAT_ID")
    private Category category;

    public Memo() {
    }

    public Memo(String name, String content, String comment, Boolean isPrivate, User owner, Integer type, Timestamp startDate, Integer color, Category category) {
        this.name = name;
        this.content = content;
        this.comment = comment;
        this.isPrivate = isPrivate;
        this.owner = owner;
        this.type = type;
        this.startDate = startDate;
        this.color = color;
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Memo memo = (Memo) o;

        if (isPrivate != memo.isPrivate) return false;
        if (!name.equals(memo.name)) return false;
        if (!content.equals(memo.content)) return false;
        if (!comment.equals(memo.comment)) return false;
        if (!owner.equals(memo.owner)) return false;
        if (!type.equals(memo.type)) return false;
        if (!startDate.equals(memo.startDate)) return false;
        if (!color.equals(memo.color)) return false;
        return category.equals(memo.category);

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + content.hashCode();
        result = 31 * result + comment.hashCode();
        result = 31 * result + (isPrivate ? 1 : 0);
        result = 31 * result + owner.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + startDate.hashCode();
        result = 31 * result + color.hashCode();
        result = 31 * result + category.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Memo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", content='" + content + '\'' +
                ", comment='" + comment + '\'' +
                ", isPrivate=" + isPrivate +
                ", type=" + type +
                ", startDate=" + startDate +
                ", color=" + color +
                ", category=" + category +
                '}';
    }
}
