package ru.tatskill.pamyatka.web.dao.impl;

import org.springframework.stereotype.Repository;
import ru.tatskill.pamyatka.web.dao.UserDAO;
import ru.tatskill.pamyatka.web.model.User;

/**
 * Created by Admin on 02.08.2015.
 */
@Repository
public class UserDAOImpl extends DAOImpl<User, Integer> implements UserDAO {
}
