package ru.tatskill.pamyatka.web.model;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Admin on 13.07.2015.
 */
@Entity
@Table(name = "Category")
public class Category {
    @Id
    @Column(name = "CAT_ID")
    @GeneratedValue
    private Integer id;

    @Column(name = "CAT_NAME", nullable = false, unique = true)
    private String name;

    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
    private Set<Template> templates;

    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
    private Set<Memo> memos;

    public Category() {
    }

    public Category(String name, Set<Template> templates, Set<Memo> memos) {
        this.name = name;
        this.templates = templates;
        this.memos = memos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Template> getTemplates() {
        return templates;
    }

    public void setTemplates(Set<Template> templates) {
        this.templates = templates;
    }

    public Set<Memo> getMemos() {
        return memos;
    }

    public void setMemos(Set<Memo> memos) {
        this.memos = memos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        return name.equals(category.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
