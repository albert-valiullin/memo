package ru.tatskill.pamyatka.web.controller;

import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Admin on 13.07.2015.
 */
@Controller
public class IndexController {

//    private static final Logger logger = Logger.getLogger(IndexController.class);

    @RequestMapping(value = "/")
    public String indexGet(Model model) {

        String name = null;
        Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal(); //get logged in username
        if (user instanceof org.springframework.security.core.userdetails.User) {
            name = ((org.springframework.security.core.userdetails.User) user).getUsername();
        } else {
            name = (String) user;
        }
        model.addAttribute("username", name);


        return "index";
    }

    @RequestMapping(value = "/login")
    public String loginGet() {
        return "login";
    }
    @RequestMapping(value = "/error403")
    public String error403() {
        return "error403";
    }
    @RequestMapping(value = "/registration")
    public String registr() {
        return "registr";
    }
}
