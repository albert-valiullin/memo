package ru.tatskill.pamyatka.web.dao.impl;

import org.springframework.stereotype.Repository;
import ru.tatskill.pamyatka.web.dao.CategoryDAO;
import ru.tatskill.pamyatka.web.model.Category;

/**
 * Created by Admin on 02.08.2015.
 */
@Repository
public class CategoryDAOImpl extends DAOImpl<Category, Integer> implements CategoryDAO {
}
