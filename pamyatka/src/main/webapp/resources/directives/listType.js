/**
 * Created by Admin on 05.09.2015.
 */
app.directive('listType', function() {
    return {
        restrict: 'E',
        scope: true,
        replace: true,
        template: '<ng-transclude/>',
        transclude: true,
        link: function($scope, $element, $attrs) {
            var tag = $scope.memoType.toLowerCase() === 'ol' ? '<ol>' : '<ul>';
            $element.wrap(tag);
        }
    };
});