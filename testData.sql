﻿INSERT INTO category VALUES (0, 'Not Specified');
INSERT INTO user(user_id, user_username, user_email, user_password) VALUES (1, 'Admin', 'admin@example.com', 'admin');

INSERT INTO memo(memo_id, memo_name, memo_content, memo_comment, memo_private, memo_user_id, memo_type, memo_color, memo_cat_id)  VALUES (
    1,
    'First Memo (Unordered point)',
    '[{text: ''Point 1'', importance: 0},
      {text: ''Point 2'', importance: 0},
      {text: ''Point 3'', importance: 0},
      {text: ''Point 4'', importance: 0},
      {text: ''Point 5'', importance: 0}]',
    'No comment',
    1,
    1,
    0,
    0,
    0
);
INSERT INTO memo(memo_id, memo_name, memo_content, memo_comment, memo_private, memo_user_id, memo_type, memo_color, memo_cat_id)  VALUES (
  2,
  '2d Memo (Ordered point)',
  '[{text: ''Point 1'', importance: 0},
    {text: ''Point 2'', importance: 0},
    {text: ''Point 3'', importance: 0},
    {text: ''Point 4'', importance: 0},
    {text: ''Point 5'', importance: 0}]',
  'No comment',
  1,
  1,
  1,
  0,
  0
);
INSERT INTO memo(memo_id, memo_name, memo_content, memo_comment, memo_private, memo_user_id, memo_type, memo_color, memo_cat_id)  VALUES (
    3,
    'Third Memo (Unordered checklist)',
    '[{text: ''Check 1'', importance: 0, done: false},
      {text: ''Check 2'', importance: 0, done: false},
      {text: ''Check 3'', importance: 0, done: false},
      {text: ''Check 4'', importance: 0, done: false},
      {text: ''Check 5'', importance: 0, done: false}]',
    'No comment',
    1,
    1,
    2,
    0,
    0
);
INSERT INTO memo(memo_id, memo_name, memo_content, memo_comment, memo_private, memo_user_id, memo_type, memo_color, memo_cat_id)  VALUES (
    4,
    '4th Memo (Ordered checklist)',
    '[{text: ''Step 1'', importance: 0, done: false},
      {text: ''Step 2'', importance: 0, done: false},
      {text: ''Step 3'', importance: 0, done: false},
      {text: ''Step 4'', importance: 0, done: false},
      {text: ''Step 5'', importance: 0, done: false}]',
    'No comment',
    1,
    1,
    3,
    0,
    0
);
